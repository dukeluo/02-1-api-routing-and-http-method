package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class ControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    // 2.1, 2.2
    @Test
    void should_return_status_200_with_message_when_send_a_get_request_with_the_id() throws Exception {
        long id1 = 2;
        long id2 = 23;

        mockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/users/%d/books", id1)))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
        mockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/users/%d/books", id2)))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 23"));
    }

    // 2.3
    @Test
    void should_prove_the_method_with_no_variable_will_be_called_first() throws Exception {
        ResultActions perform = mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"));
        MockHttpServletResponse response = perform.andReturn().getResponse();

        assertEquals(200, response.getStatus());
        assertEquals("There is no variable", response.getContentAsString());
        assertNotEquals("There are variables here", response.getContentAsString());
    }

    // 2.4
    @Test
    void should_prove_the_question_mark_wildcard_will_match_one_char() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/0/x"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a char"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/0/xx"))
                .andExpect(MockMvcResultMatchers.status().is(404));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/0/"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    // 2.5.1
    @Test
    void should_prove_the_star_mark_wildcard_will_match_one_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/1024/xxx"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a star"));
    }

    // 2.5.2, 2.5.3
    @Test
    void should_prove_the_star_mark_wildcard_can_match_middle_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/1/xxx/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a star in the middle path"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/1/test"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    // 2.5.4
    @Test
    void should_prove_the_star_mark_wildcard_can_use_in_middle_path_text() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/2/pre-xxx-sub/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a star in the path word"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/2/pre-xxx/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a star in the path word"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/2/xxx-sub/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a star in the path word"));
    }

    // 2.6
    @Test
    void should_prove_the_double_star_mark_wildcard_can_use_in_middle_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/3/xxx/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There are two stars in the middle path"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/3/test"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There are two stars in the middle path"));
    }

    // 2.7
    @Test
    void should_prove_the_reg_expressions_can_used() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/4/1024"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("There is a reg expression, and the id is 1024"));
    }

    // 2.8.1, 2.8.2
    @Test
    void should_prove_the_request_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user?id=1024"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The id is 1024"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    // 2.8.3
    @Test
    void should_prove_the_specified_request_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user?id=1024&name=jack"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The id is 1024, and the name is jack"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user?country=china"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}