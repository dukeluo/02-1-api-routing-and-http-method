package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    @Test
    void should_return_books_with_the_id() {
        Controller controller = new Controller();

        assertEquals("The book for user 3", controller.getBooks(3));
    }

}