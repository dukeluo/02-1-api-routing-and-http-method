package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    // 2.1, 2.2
    @GetMapping("/api/users/{id}/books")
    public String getBooks(@PathVariable("id") long ID) {
        return String.format("The book for user %d", ID);
    }

    // 2.3
    @GetMapping("/api/segments/good")
    public String testPriorityWithNoVariable() {
        return "There is no variable";
    }

    @GetMapping("/api/segments/{segmentName}")
    public String testPriorityWithVariable(@PathVariable("segmentName") String name) {
        return "There are variables here";
    }

    // 2.4
    @GetMapping("/api/wildcard/0/?")
    public String testQuestionMarkWildcard() {
        return "There is a char";
    }

    // 2.5.1
    @GetMapping("/api/wildcard/1024/*")
    public String testStarMarkWildcard() {
        return "There is a star";
    }

    // 2.5.2, 2.5.3
    @GetMapping("/api/wildcard/1/*/test")
    public String testStarMarkWildcardInMiddlePath() {
        return "There is a star in the middle path";
    }

    // 2.5.4
    @GetMapping("/api/wildcard/2/pre-*-sub/test")
    public String testStarMarkWildcardInPathTextOne() {
        return "There is a star in the path word";
    }

    // 2.5.4
    @GetMapping("/api/wildcard/2/*-sub/test")
    public String testStarMarkWildcardInPathTextTwo() {
        return "There is a star in the path word";
    }

    // 2.5.4
    @GetMapping("/api/wildcard/2/pre-*/test")
    public String testStarMarkWildcardInPathTextThree() {
        return "There is a star in the path word";
    }

    // 2.6
    @GetMapping("/api/wildcard/3/**/test")
    public String testStarDoubleMarkWildcard() {
        return "There are two stars in the middle path";
    }

    // 2.7
    @GetMapping("/api/wildcard/4/{numericId:[\\d]+}")
    public String testRegExpression(@PathVariable int numericId) {
        return String.format("There is a reg expression, and the id is %d", numericId);
    }

    // 2.8.1, 2.8.2
    @GetMapping("/api/user")
    public String testRequestParam(@RequestParam("id") int numericId) {
        return String.format("The id is %d", numericId);
    }

    // 2.8.3
    @GetMapping(
            value = "/api/user",
            params = { "id", "name" })
    public String testSpecifiedRequestParam(@RequestParam("id") int numericId,
                                            @RequestParam("name") String name) {
        return String.format("The id is %d, and the name is %s", numericId, name);
    }
}
